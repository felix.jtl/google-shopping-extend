<?php declare(strict_types=1);

namespace Plugin\jtl_google_shopping_extend;

use JTL\DB\ReturnType;
use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use Plugin\jtl_google_shopping\Exportformat\Product;

/**
 * Class Bootstrap
 * @package Plugin\jtl_google_shopping_extend
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @var array|null
     */
    private $data;

    /**
     * @inheritdoc
     */
    public function boot(Dispatcher $dispatcher)
    {
        $dispatcher->listen('shop.hook.' . \HOOK_ARTIKEL_CLASS_FUELLEARTIKEL, function ($args) {
            /** @var Product $product */
            $product = $args['oArtikel'] ?? null;
            if ($product === null || \get_class($args['oArtikel']) !== Product::class || $product->kWarengruppe <= 0) {
                return;
            }
            $product->warengruppe = $this->getCommodityGroupData($product->kWarengruppe);
        });
        parent::boot($dispatcher);
    }

    /**
     * @param int $groupID
     * @return string
     */
    public function getCommodityGroupData(int $groupID): string
    {
        if ($this->data === null) {
            $this->data = [];
            $data       = $this->getDB()->query('SELECT * FROM twarengruppe', ReturnType::ARRAY_OF_OBJECTS);
            foreach ($data as $item) {
                $this->data[(int)$item->kWarengruppe] = $item->cName;
            }
        }

        return $this->data[$groupID] ?? '';
    }
}
